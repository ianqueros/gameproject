#pragma once
#include "Global.h"

enum TimerEvent {
	CREATE_TIMER, STOP_APPLICATION, SHOW_TIMERS
};

class TimerHandler;

class TimerCommunicator
{
public:
	int event;
	void *data;
	ALLEGRO_EVENT_QUEUE* timerQueue;
	ALLEGRO_EVENT timerEvent;
	ALLEGRO_TIMER* handlerTimer;

	TimerCommunicator();
	~TimerCommunicator();
};

