#include "App.h"
#include "EventHandler.h"

EventHandler* EventHandler::eventHandSingleton = nullptr;

EventHandler::EventHandler(App* app)
{
	_appPath = app;
	_eventQueue = al_create_event_queue();

	al_register_event_source(_eventQueue, al_get_keyboard_event_source());
	al_register_event_source(_eventQueue, al_get_mouse_event_source());
}


EventHandler::~EventHandler()
{
	al_destroy_event_queue(_eventQueue);
}

void EventHandler::wait()
{
	al_wait_for_event(_eventQueue, &_event);
}

EventHandler * EventHandler::createHandler(App* app)
{
	if (EventHandler::eventHandSingleton == nullptr) {
		EventHandler::eventHandSingleton = new EventHandler(app);
	}
	return EventHandler::eventHandSingleton;
}


