#include "StrManipulator.h"



StrManipulator::StrManipulator()
{
}


StrManipulator::~StrManipulator()
{
}

std::list<std::string> StrManipulator::split(std::string& in,std::string splitBy)
{
	std::list<std::string> splitList;
	if (in.length() < splitBy.length()) {
		return  splitList;
	}
	else if (in.length() == splitBy.length()) {
		splitList.push_back("");
		splitList.push_back("");
		return splitList;
	}
	std::string strAux;
	for (size_t i = 0; i < in.length() - splitBy.length() + 1; i++) {
		if (in.substr(i, splitBy.length()) == splitBy) {
			strAux = in.substr(0, i);
			splitList.push_back(strAux);
			in.erase(0,i + splitBy.length());
			i = -1;
		}
		if (i == in.length() - splitBy.length()) {
			splitList.push_back(in);
		}
	}
	return splitList;
}

void StrManipulator::test()
{
	int passed = 0;
	std::cout << "testing slit" << std::endl;
	std::string testString = "oi quero testar";
	std::list<std::string> li;
	std::list<std::string>::iterator ite;
	li = StrManipulator::split(testString, " ");
	ite = li.begin();

	if ((*ite) == "oi") {
		ite++;
		if ((*ite) == "quero"){
			ite++;
			if ((*ite) == "testar") {
				passed++;
			}
		}
	}
	if (passed == 1) {
		std::cout << "passed";
	}
	int a;
	std::cin >> a;
}
