#include "EntityHandler.h"

EntityHandler* EntityHandler::entityHandSingleton = nullptr;

EntityHandler::EntityHandler(App* app)
{
	_appPath = app;
}


EntityHandler::~EntityHandler()
{
	clear();
}

void EntityHandler::createEntity(Entity * ent)
{
	allEntities.push_back(ent);
}

void EntityHandler::initialize()
{
	mainPlayer = new Player(100, 100, 0);
	allEntities.push_back(new Player(400, 400, 1));
}

void EntityHandler::update()
{
	mainPlayer->update();
	for (std::list<Entity*>::iterator it = allEntities.begin(); it != allEntities.end(); it++) {
		if ((*it)->getActive()) {
			(*it)->update();
		}
	}

}

void EntityHandler::draw()
{
	mainPlayer->draw();
	for (std::list<Entity*>::iterator it = allEntities.begin(); it != allEntities.end(); it++) {
		if ((*it)->getActive()) {
			(*it)->draw();
		}
	}
}

void EntityHandler::reset()
{
	clear();
	initialize();
}

void EntityHandler::clear()
{
	delete mainPlayer;
	std::list<Entity*>::iterator it;
	while (allEntities.size()) {
		it = allEntities.begin();
		delete (*it);
		allEntities.pop_front();
	}
}
