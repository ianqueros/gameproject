#include "App.h"
#include "TimerHandler.h"

int64_t TimerHandler::mTCount = 0;

void* timerThreadMain(ALLEGRO_THREAD* thread, void* data) {
	std::cout << "Thread created opening its console" << std::endl;
	TimerHandler * _timerPath = TimerHandler::createHandler(nullptr);
	TimerCommunicator* communicator = (TimerCommunicator*)data;
	CConsoleLogger* timerConsole = _timerPath->getLogger();
	std::list<ALLEGRO_TIMER*>* timerList = _timerPath->getTimerList();
	timerConsole->printf("Thread initiated with success\n");
	bool done = false;
	int i;

	al_start_timer(communicator->handlerTimer);
	while (!done) {
		al_wait_for_event(communicator->timerQueue, &communicator->timerEvent);
		switch (communicator->timerEvent.type) {
		case ALLEGRO_EVENT_TIMER:
			if (communicator->event != -1) {
				switch (communicator->event) {
				case CREATE_TIMER:
					timerConsole->printf("Timer created\n");
					break;
				case SHOW_TIMERS:
					timerConsole->printf("List of timers with size:%d\n", timerList->size());
					i = 0;
					try {
						if (!timerList->empty()) {
							for (
								std::list<ALLEGRO_TIMER*>::iterator timerIte = timerList->begin();
								timerIte != timerList-> end();
								timerIte++) {
								timerConsole->printf(
									"Timer %d with speed of %f and status:(%s) and count %d\n",
									i, al_get_timer_speed((*timerIte)),
									al_get_timer_started((*timerIte)) ? "started" : "stopped",
									al_get_timer_count((*timerIte))
								);
								i++;
							}
						}
					}
					catch (std::exception excpt) {
						std::cout << "Exception thrown when listing timers:" << excpt.what() << std::endl;
					}
					break;
				case STOP_APPLICATION:
					timerConsole->printf("Closing Timer Thread Application\n");
					done = true;
				}
				communicator->event = -1;
			}
			break;
		}
	}

	return nullptr;
}

TimerHandler* TimerHandler::timerHandSingleton = nullptr;

TimerHandler::TimerHandler(App* app)
{
	_appPath = app;
	_mainTimer = al_create_timer(1.0 / INITIAL_FPS);
	al_register_event_source(
		_appPath->getEventHandler()->getQueue(),
		al_get_timer_event_source(_mainTimer)
	);
	timerConsole.Create("Timer Console");
	timerThread = al_create_thread(timerThreadMain, &communicator);
	al_start_thread(timerThread);
}


TimerHandler::~TimerHandler()
{
	std::list<ALLEGRO_TIMER*>::iterator it;
	while (timerList.size()) {
		try {
			it = timerList.begin();
			timerConsole.printf("Destroying timer %p with speed of %.4f ", (*it), al_get_timer_speed((*it)));
			al_destroy_timer((*it));
			timerList.pop_front();
		}
		catch (std::exception excpt) {
			std::cout << "Exception thrown when deleting timers:" << excpt.what() << std::endl;
		}
	}
	communicator.event = STOP_APPLICATION;
	al_join_thread(timerThread, NULL);
	al_destroy_thread(timerThread);
	timerConsole.Close();
}

void TimerHandler::tick()
{
	mTCount = al_get_timer_count(_mainTimer);
}

void TimerHandler::startApp()
{
	al_start_timer(_mainTimer);
}

void TimerHandler::pauseApp()
{
	al_stop_timer(_mainTimer);
}

ALLEGRO_TIMER* TimerHandler::createTimer(double seconds)
{
	timerConsole.printf("Creating timer with %f seconds\n", seconds);
	ALLEGRO_TIMER* newTimer = al_create_timer(seconds);
	timerList.push_back(newTimer);
	communicator.event = CREATE_TIMER;
	return newTimer;
}

TimerHandler * TimerHandler::createHandler(App* app)
{
	if (TimerHandler::timerHandSingleton == nullptr) {
		TimerHandler::timerHandSingleton = new TimerHandler(app);
	}
	return TimerHandler::timerHandSingleton;
}