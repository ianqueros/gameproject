#include "Entity.h"
#include "CollisionBox.h"
#include "EntityHandler.h"



Entity::Entity(float px, float py):
	active(false), x(px), y(py), vx(0), vy(0), ax(0), ay(0)
{
	cam = Camera::getCamera();
	_entPath = EntityHandler::createHandler(nullptr);
	collision = new bool[4];
	for (int i = 0; i < 4; i++) {
		collision[i] = false;
	}
	active = true;
}


Entity::~Entity()
{
	delete[] collision;
}
