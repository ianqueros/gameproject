#pragma once
#include <iostream>
#include <string>
#include <list>

class StrManipulator
{
public:
	StrManipulator();
	~StrManipulator();

	static std::list<std::string> split(std::string& in, std::string splitBy);
	static void test();
};

