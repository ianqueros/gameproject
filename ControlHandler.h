#pragma once

#include "Global.h"
#include "DisplayHandler.h"

class ControlHandler
{
private:
	App* _appPath;
	static ControlHandler* controlHandSingleton;

	bool* keys;
	int mx, my;
	bool mPressed;
	ControlHandler(App* app);
public:
	~ControlHandler();

	void keyDown(ALLEGRO_EVENT event);
	void keyUp(ALLEGRO_EVENT event);

	void mouse(int x, int y) { mx = x; my = y; }
	void mousePressed(bool p) { mPressed = p; }

	static ControlHandler* createHandler(App* app);

	//Getters & Setters
	int mouseX() { return mx; }
	int mouseY() { return my; }
	bool get(int key);
	bool getMP() { return mPressed; }
};

