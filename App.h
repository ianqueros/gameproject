#pragma once

#include "SimulationGameState.h"

class App
{
private:
	DisplayHandler *_display;
	EventHandler *_eventHandler;
	TimerHandler * _timerHandler;
	ControlHandler * _controlHandler;
	GameState * _state;
	std::list<GameState*> _previousStates;

	bool changedState;

	void init();
	void mainLoop();

public:
	App();
	~App();

	bool done;
	bool updating;
	bool draw;
	bool drawing;
	
	//Getters & Setters
	EventHandler* getEventHandler() const {
		return _eventHandler;
	}
	DisplayHandler* getDisplayHandler() const {
		return _display;
	}
	ControlHandler* getControlHandler() const {
		return _controlHandler;
	}
	TimerHandler* getTimerHandler() const {
		return _timerHandler;
	}
	GameState* getState() const {
		return _state;
	}
	void setState(enum States newState, bool saveState);
};

