#pragma once
class Camera
{
private:
	static Camera* camSingleton;
public:
	float x, y, vx, vy, ax, ay;

	Camera();
	~Camera();

	void reset(float px, float py);

	float posX(float px);
	float posY(float py);

	static Camera* getCamera() {
		if (Camera::camSingleton == nullptr) {
			Camera::camSingleton = new Camera();
		}
		return Camera::camSingleton;
	}
};

