#pragma once

#include "Global.h"
#include "TimerCommunicator.h"
#include "EventHandler.h"

struct TimerKey {
	int key;
	ALLEGRO_TIMER* value;
};

class App;

class TimerHandler
{
private:
	App* _appPath;
	ALLEGRO_TIMER * _mainTimer;
	static int64_t mTCount;
	std::list<ALLEGRO_TIMER*> timerList;
	static TimerHandler* timerHandSingleton;
	CConsoleLogger timerConsole;
	ALLEGRO_THREAD * timerThread;
	TimerCommunicator communicator;

	TimerHandler(App* app);
public:
	~TimerHandler();

	static int64_t mainTimerCount() { return TimerHandler::mTCount; }

	void tick();
	void startApp();
	void pauseApp();

	void showTimers() {
		communicator.event = SHOW_TIMERS;
	}

	ALLEGRO_TIMER* createTimer(double seconds);

	static TimerHandler* createHandler(App* app);
	//Getters& Setters
	CConsoleLogger* getLogger() {
		return &timerConsole;
	}
	std::list<ALLEGRO_TIMER*>* getTimerList() {
		return &timerList;
	}
};

