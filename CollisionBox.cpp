#include "Entity.h"
#include "CollisionBox.h"



CollisionBox::CollisionBox(Entity* o, CollisionType t, float ux, float uy, float dx, float dy, int d):
	owner(o), type(t), upperX(ux), upperY(uy), downX(dx), downY(dy), damage(d), 
	color(al_map_rgba(rand()%206 + 50, rand() % 206 + 50, rand() % 206 + 50, 220))
{
	show = true;
	cam = Camera::getCamera();
}


CollisionBox::~CollisionBox()
{
}

void CollisionBox::calculate()
{
	maxX = std::abs(upperX) > std::abs(downX) ? upperX : downX;
	maxY = std::abs(upperY) > std::abs(downY) ? upperY : downY;
	maxDistance = std::sqrt(maxX*maxX + maxY * maxY);
}

bool CollisionBox::withinRange(CollisionBox * box)
{
	float x = getMidX() - box->getMidX();
	float y = getMidY() - box->getMidY();
	if (std::sqrt(x * x + y * y) <= maxDistance + box->getMaxDist()) return true;
	return false;
}

bool CollisionBox::checkSquareCollision(CollisionBox * box)
{
	if (getUX() <= box->getDX() &&
		getDX() >= box->getUX() &&
		getUY() <= box->getDY() &&
		getDY() >= box->getUY())
		return true;
	return false;
}

CollisionSide CollisionBox::checkSide(CollisionBox * box)
{
	return CollisionSide();
}

void CollisionBox::draw()
{
	if (show) {
		al_draw_filled_rectangle(cam->posX(getUX()), cam->posY(getUY()),
			cam->posX(getDX()), cam->posY(getDY()), color);
	}
}
