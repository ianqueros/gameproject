#pragma once

#include <string>
#include <list>
#include <iostream>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_video.h>
#include <allegro5/allegro_audio.h>
#include <set>

#include "StrManipulator.h"
#include "ConsoleLogger.h"

#define INITIAL_FPS 30

#define NUMBER_PLAYER_KEYS 4
#define NUMBER_OF_KEYS 9
//All available keys
enum Keys {
	UP, DOWN, LEFT, RIGHT, W, S, A, D,ESCAPE
};

enum States {
	SIMULATION, MENU
};