#include "App.h"
#include "DisplayHandler.h"

DisplayHandler* DisplayHandler::displayHandSingleton = nullptr;

DisplayHandler::DisplayHandler(App* app)
{
	_appPath = app;
	_display = al_create_display(WIDTH, HEIGHT);
	//Registering display in event queue
	al_register_event_source(
		_appPath->getEventHandler()->getQueue(),
		al_get_display_event_source(_display)
	);
}


DisplayHandler::~DisplayHandler()
{
	al_destroy_display(_display);
}

void DisplayHandler::flip() {
	al_flip_display();
}

DisplayHandler * DisplayHandler::createHandler(App* app)
{
	if (DisplayHandler::displayHandSingleton == nullptr) {
		DisplayHandler::displayHandSingleton = new DisplayHandler(app);
	}
	return DisplayHandler::displayHandSingleton;
}
