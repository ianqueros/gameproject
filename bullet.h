#pragma once

#include "Player.h"
#include "CollisionBox.h"

class bullet :
	public Entity
{
private:
	CollisionBox * damageArea;
public:
	bullet(float x, float y, float velocity, double direction, float damage);
	~bullet();

	void activate();
	void update();
	void draw();
	void reset(float px, float py);
	void checkCollision(CollisionBox* box);

};

