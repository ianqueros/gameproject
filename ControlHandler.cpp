#include "App.h"
#include "ControlHandler.h"


ControlHandler* ControlHandler::controlHandSingleton = nullptr;

ControlHandler::ControlHandler(App* app):
	mx(0), my(0), mPressed(false)
{
	
	_appPath = app;
	keys = new bool[NUMBER_OF_KEYS];
	for (int i = 0; i < NUMBER_OF_KEYS; i++) {
		keys[i] = false;
	}
}


ControlHandler::~ControlHandler()
{
	delete[] keys;
}

void ControlHandler::keyDown(ALLEGRO_EVENT event)
{
	switch (event.keyboard.keycode) {
	case ALLEGRO_KEY_UP:
		keys[UP] = true;
		break;
	case ALLEGRO_KEY_DOWN:
		keys[DOWN] = true;
		break;
	case ALLEGRO_KEY_LEFT:
		keys[LEFT] = true;
		break;
	case ALLEGRO_KEY_RIGHT:
		keys[RIGHT] = true;
		break;
	case ALLEGRO_KEY_W:
		keys[W] = true;
		break;
	case ALLEGRO_KEY_S:
		keys[S] = true;
		break;
	case ALLEGRO_KEY_A:
		keys[A] = true;
		break;
	case ALLEGRO_KEY_D:
		keys[D] = true;
		break;
	case ALLEGRO_KEY_ESCAPE:
		keys[ESCAPE] = true;
		break;
	}
}

void ControlHandler::keyUp(ALLEGRO_EVENT event)
{
	switch (event.keyboard.keycode) {
	case ALLEGRO_KEY_UP:
		keys[UP] = false;
		break;
	case ALLEGRO_KEY_DOWN:
		keys[DOWN] = false;
		break;
	case ALLEGRO_KEY_LEFT:
		keys[LEFT] = false;
		break;
	case ALLEGRO_KEY_RIGHT:
		keys[RIGHT] = false;
		break;
	case ALLEGRO_KEY_W:
		keys[W] = false;
		break;
	case ALLEGRO_KEY_S:
		keys[S] = false;
		break;
	case ALLEGRO_KEY_A:
		keys[A] = false;
		break;
	case ALLEGRO_KEY_D:
		keys[D] = false;
		break;
	case ALLEGRO_KEY_ESCAPE:
		keys[ESCAPE] = false;
		break;
	}
}

bool ControlHandler::get(int key)
{
	if (key < NUMBER_OF_KEYS)
		return keys[key];
	return false;
}

ControlHandler * ControlHandler::createHandler(App* app)
{
	if (ControlHandler::controlHandSingleton == nullptr) {
		ControlHandler::controlHandSingleton = new ControlHandler(app);
	}
	return ControlHandler::controlHandSingleton;
}
