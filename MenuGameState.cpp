#include "MenuGameState.h"
#include "App.h"


MenuGameState::MenuGameState(App* app, ControlHandler* control):
	GameState(app, control)
{
}


MenuGameState::~MenuGameState()
{
}

void MenuGameState::update()
{
	_appPath->setState(SIMULATION, false);
}

void MenuGameState::draw()
{
	al_draw_filled_circle(300, 300, 60, al_map_rgb(244, 0, 0));
}

void MenuGameState::enter()
{
}

void MenuGameState::leave()
{
}
