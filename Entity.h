#pragma once
#include "Global.h"
#include "Camera.h"


enum Position {POSITION, VELOCITY, ACCELERATION};
enum Axis {X, Y};

class CollisionBox;
class EntityHandler;

class Entity
{
protected:
	float x, y, vx, vy, ax, ay; // position, velocity and acceleration
	bool active;
	bool * collision;
	Camera* cam;
	EntityHandler* _entPath;
public:
	Entity(float px, float py);
	virtual ~Entity();

	virtual void activate() = 0;
	virtual void update() = 0;
	virtual void draw() = 0;
	virtual void reset(float px, float py) = 0;
	virtual void checkCollision(CollisionBox* box) = 0;

	//Getter & Setters
	bool getActive() { return active; }
	float getX() { return x; }
	float getY() { return y; }
	float getVX() { return vx; }
	float getVY() { return vy; }
	float getAX() { return ax; }
	float getAY() { return ay; }
	virtual void setVX(float v) { vx = v; }
	virtual void setVY(float v) { vy = v; }
	virtual void setAX(float a) { ax = a; }
	virtual void setAY(float a) { ay = a; }
};

