#include "TimerCommunicator.h"



TimerCommunicator::TimerCommunicator():
	event(-1)
{
	timerQueue = al_create_event_queue();
	handlerTimer = al_create_timer(1.0);
	al_register_event_source(timerQueue, al_get_timer_event_source(handlerTimer));
}


TimerCommunicator::~TimerCommunicator()
{
}
