#pragma once

#include "Global.h"
#include "TimerHandler.h"
#define WIDTH 640
#define HEIGHT 480

class DisplayHandler
{
private:
	App* _appPath;
	ALLEGRO_DISPLAY * _display;
	static DisplayHandler* displayHandSingleton;
	DisplayHandler(App* app);
public:
	~DisplayHandler();
	void flip();
	static DisplayHandler* createHandler(App* app);

	const ALLEGRO_DISPLAY* getDisplay() const {
		return _display;
	}
};

