#include "Camera.h"

Camera* Camera::camSingleton = nullptr;

Camera::Camera():
	x(0), y(0)
{
}


Camera::~Camera()
{
}

void Camera::reset(float px, float py) {
	x = px;
	y = py;
}

float Camera::posX(float px)
{
	return px - x;
}

float Camera::posY(float py)
{
	return py - y;
}
