#pragma once

#include "Entity.h"
class ControlHandler;
class bullet;

class Player: public Entity
{
private:
	int player;
	float fullLife;
	float life;
	ALLEGRO_TIMER* bulletTimer;
	double bulletTime;
	ControlHandler* _controlPath;
	CollisionBox * body;
	std::list<CollisionBox*> allBoxes;
public:
	Player(float px, float py, int player);
	~Player();

	void activate();
	void update();
	void draw();
	void reset(float px, float py);
	void checkCollision(CollisionBox* box);
};

