#include "App.h"

App::App()
{
	init();
}


App::~App()
{
	delete _display;
	delete _eventHandler;
	delete _timerHandler;
	delete _state;
}

void App::init() {
	al_init();
	al_init_primitives_addon();
	al_install_keyboard();
	al_install_mouse();

	//Initializing Handlers
	_eventHandler = EventHandler::createHandler(this);
	_timerHandler = TimerHandler::createHandler(this);
	_display = DisplayHandler::createHandler(this);
	_controlHandler = ControlHandler::createHandler(this);

	//Setting first state
	_state = new MenuGameState(this, _controlHandler);
	_state->enter();

	//Setting app variables
	changedState = false;
	done = false;
	updating = false;
	draw = false;
	drawing = false;

	
	_timerHandler->startApp();
	mainLoop();
}
void App::mainLoop()
{
	while (!done) {
		changedState = false;
		_eventHandler->wait();
		switch (_eventHandler->getEvent().type) {
		case ALLEGRO_EVENT_DISPLAY_CLOSE:
			done = true;
			break;
		case ALLEGRO_EVENT_TIMER:
			_timerHandler->tick();
			if (!changedState && !updating) {
				_state->update();
				draw = true;
			}
			break;
		case ALLEGRO_EVENT_KEY_DOWN:
			_controlHandler->keyDown(_eventHandler->getEvent());
			break;
		case ALLEGRO_EVENT_KEY_UP:
			_controlHandler->keyUp(_eventHandler->getEvent());
			break;
		case ALLEGRO_EVENT_MOUSE_AXES:
			_controlHandler->mouse(_eventHandler->getEvent().mouse.x, _eventHandler->getEvent().mouse.y);
			break;
		case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
			_controlHandler->mouse(_eventHandler->getEvent().mouse.x, _eventHandler->getEvent().mouse.y);
			_controlHandler->mousePressed(true);
			break;
		case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
			_controlHandler->mouse(_eventHandler->getEvent().mouse.x, _eventHandler->getEvent().mouse.y);
			_controlHandler->mousePressed(false);
		}

		if (draw && !changedState && !drawing) {
			_state->draw();
			_display->flip();
			draw = false;
		}

	}
}

void App::setState(enum States newState, bool saveState) {
	_state->leave();
	if (saveState) {
		_previousStates.push_back(_state);
	}
	else {
		delete _state;
	}
	GameState* newStateptr = nullptr;
	switch (newState) {
	case MENU:
		newStateptr = new MenuGameState(this, _controlHandler);
		break;
	case SIMULATION:
		newStateptr = new SimulationGameState(this, _controlHandler);
		break;
	}
	newStateptr->enter();
	_state = newStateptr;
	changedState = true;
}
