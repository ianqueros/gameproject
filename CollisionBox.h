#pragma once
#include "Global.h"
#include "Entity.h"

enum CollisionType {WALL, BODY, DAMAGE};
enum CollisionSide { RIGHT_C, LEFT_C, UP_C, DOWN_C };

//class Entity;

class CollisionBox
{
private:
	CollisionType type;
	float upperX, upperY;
	float downX, downY;
	float maxX, maxY, maxDistance;
	int damage;
	bool show;
	ALLEGRO_COLOR color;
	Entity* owner;
	Camera* cam;
public:
	CollisionBox(Entity* o, CollisionType t, float ux, float uy, float dx, float dy, int d);
	~CollisionBox();

	void calculate();

	bool withinRange(CollisionBox * box);
	bool checkSquareCollision(CollisionBox * box);
	CollisionSide checkSide(CollisionBox * box);
	void draw();

	//Getters & Setters
	Entity * getOwner() {
		return owner;
	}
	float getUX() { return owner->getX() - upperX; }
	float getUY() { return owner->getY() - upperY; }
	float getDX() { return owner->getX() + downX; }
	float getDY() { return owner->getY() + downY; }
	float getMaxX() { return maxX; }
	float getMaxY() { return maxY; }
	float getMidX() { return (getUX() + getDX()) / 2.0; }
	float getMidY() { return (getUY() + getDY()) / 2.0; }
	float getMaxDist() { return maxDistance; }

};

