#pragma once

#include "ControlHandler.h"
#include "bullet.h"

class EntityHandler
{
private:
	App * _appPath;
	Player * mainPlayer;
	std::list<Entity*> allEntities;
	static EntityHandler* entityHandSingleton;
	EntityHandler(App* app);
public:
	
	~EntityHandler();

	void createEntity(Entity* ent);

	void initialize();
	void update();
	void draw();
	void reset();
	void clear();

	static EntityHandler* createHandler(App* app) {
		if (EntityHandler::entityHandSingleton == nullptr) {
			EntityHandler::entityHandSingleton = new EntityHandler(app);
			return EntityHandler::entityHandSingleton;
		}
		return EntityHandler::entityHandSingleton;
	}
};

