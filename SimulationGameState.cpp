#include "SimulationGameState.h"
#include "App.h"



SimulationGameState::SimulationGameState(App* app, ControlHandler* control):
	GameState(app, control)
{
	_timerPath = TimerHandler::createHandler(app);
	cam = Camera::getCamera();
	_entPath = EntityHandler::createHandler(app);
	_entPath->initialize();

	//simulationTimer = _timerPath->createTimer(1.0 / 60);
	/*counter = 0;
	countTo = 0;
	x = y = 0;
	run = false;*/

}


SimulationGameState::~SimulationGameState()
{
}


void SimulationGameState::update()
{
	_appPath->updating = true;
	/*std::cout << "counter:" << counter++ << std::endl;
	if (countTo == counter) {
		run = false;
		countTo = 0;
	}
	if (!run) {
		_appPath->getTimerHandler()->pauseApp();
		_timerPath->pauseApp();
		std::cin.getline(charInput, sizeof(charInput));
		std::string str(charInput);
		userInput = str;
		std::list<std::string> strList = StrManipulator::split(userInput, " ");
		std::list<std::string>::iterator parsedStr = strList.begin();
		if ((*parsedStr) == "run") {
			parsedStr++;
			if (parsedStr != strList.end()) {
				try {
					countTo = std::stoi((*parsedStr)) + counter - 1;
					std::cout << "Counting to " << countTo << std::endl;
					run = true;
				}
				catch (std::exception excep) {
					std::cout << "Invalid argument, exception thrown:" << excep.what() << std::endl;
					countTo = 0;
					run = false;
				}
			}
			else {
				run = true;
			}
			
		}
		else if ((*parsedStr) == "createTimer") {
			parsedStr++;
			try {
				double timerCount = std::stod((*parsedStr));
				simulationTimer = _timerPath->createTimer(timerCount);
			}
			catch (std::exception except) {
				std::cout << "Exception thrown:" << except.what() << std::endl;
			}
		}
		else if ((*parsedStr) == "showTimers") {
			_timerPath->showTimers();
		}
		else if ((*parsedStr) == "startTimer") {
			al_start_timer(simulationTimer);
		}
		else if ((*parsedStr) == "stopTimer") {
			al_stop_timer(simulationTimer);
		}
		else if ((*parsedStr) == "end") {
			_appPath->done = true;
		}
		else {
			std::cout << "unrecognized command" << std::endl;
		}
		_appPath->getTimerHandler()->startApp();
	}
	if (_controlPath->get(LEFT)) {
		x -= 3;
	}
	if (_controlPath->get(RIGHT)) {
		x += 3;
	}
	if (_controlPath->get(UP)) {
		y -= 3;
	}
	if (_controlPath->get(DOWN)) {
		y += 3;
	}


	if (_controlPath->get(ESCAPE)) {
		run = false;
		countTo = 0;
	}*/

	_entPath->update();

	_appPath->updating = false;
}

void SimulationGameState::draw()
{
	_appPath->drawing = true;

	al_clear_to_color(al_map_rgb(255, 255, 255));
	_entPath->draw();

	_appPath->drawing = false;
}

void SimulationGameState::enter()
{
}

void SimulationGameState::leave()
{
}

