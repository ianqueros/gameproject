#pragma once

#include "GameState.h"

class MenuGameState : public GameState
{
public:
	MenuGameState(App* app, ControlHandler* control);
	~MenuGameState();

	void update();
	void draw();
	void enter();
	void leave();

};

