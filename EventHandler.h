#pragma once
#include "Global.h"

class App;

class EventHandler
{
private:
	App* _appPath;
	ALLEGRO_EVENT_QUEUE * _eventQueue;
	ALLEGRO_EVENT _event;
	static EventHandler* eventHandSingleton;
	EventHandler(App* app);
public:
	~EventHandler();
	void wait();

	static EventHandler* createHandler(App* app);

	//Getters & Setters
	ALLEGRO_EVENT_QUEUE* getQueue()const {
		return _eventQueue;
	}
	ALLEGRO_EVENT getEvent()const {
		return _event;
	}
};

