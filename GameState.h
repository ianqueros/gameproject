#pragma once

#include "Global.h"
#include "EntityHandler.h"

class App;

class GameState
{
protected:
	App* _appPath;
	ControlHandler* _controlPath;
	int type;
public:
	GameState(App* app, ControlHandler* control);
	virtual ~GameState();

	virtual void update() = 0;
	virtual void draw() = 0;
	virtual void enter() = 0;
	virtual void leave() = 0;
};

