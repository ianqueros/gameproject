#include "bullet.h"


bullet::bullet(float x, float y, float velocity, double direction, float damage):
	Entity(x, y)
{
	std::cout << "Creating bullet at position x: " << x << " y:" << y << " , ang:" << direction * 180 / ALLEGRO_PI << " , vel:" << velocity << " and damage:" << damage << std::endl;
	active = true;
	damageArea = new CollisionBox(this, BODY, 5,5,5,5, 10);
	vx = velocity * std::cos(direction);
	vy = velocity * std::sin(direction);
}

bullet::~bullet()
{
	delete damageArea;
}

void bullet::activate()
{
}

void bullet::update()
{
	x += vx;
	y += vy;
}

void bullet::draw()
{
	al_draw_filled_circle(cam->posX(x), cam->posY(y), 5, al_map_rgb(100, 2, 245));
}

void bullet::reset(float px, float py)
{
}

void bullet::checkCollision(CollisionBox * box)
{
}
