#include "Player.h"
#include "CollisionBox.h"
#include "ControlHandler.h"
#include "bullet.h"
#include "EntityHandler.h"

Player::Player(float px, float py, int pl):
	Entity(px, py)
{
	_controlPath = ControlHandler::createHandler(nullptr);
	fullLife = 100;
	body = new CollisionBox(this, BODY, 10, 20, 10, 20, 0);
	allBoxes.push_back(body);
	player = pl;
}


Player::~Player()
{
	delete body;

}

void Player::activate()
{
	life = fullLife;
}

void Player::update()
{
	if (_controlPath->get(LEFT + player * NUMBER_PLAYER_KEYS)) {
		vx = -3;
	}
	if (_controlPath->get(RIGHT + player * NUMBER_PLAYER_KEYS)) {
		vx = 3;
	}
	if (_controlPath->get(UP + player * NUMBER_PLAYER_KEYS)) {
		vy = -3;
	}
	if (_controlPath->get(DOWN + player * NUMBER_PLAYER_KEYS)) {
		vy = 3;
	}

	if (_controlPath->getMP()) {
		double direction = std::atan((_controlPath->mouseY() - y) / (_controlPath->mouseX() - x));
		direction += _controlPath->mouseX() < x ? ALLEGRO_PI : 0;
		_entPath->createEntity(
			static_cast<Entity*> (new bullet(x, y, 6, direction, 10))
		);
	}

	vx += ax;
	vy += ay;
	x += vx;
	y += vy;

	ax = ay = vx = vy = 0;
}

void Player::draw()
{
	body->draw();
}

void Player::reset(float px, float py)
{
	life = fullLife;
	x = px;
	y = py;
	vx = vy = ax = ay = 0;
}

void Player::checkCollision(CollisionBox * box)
{
}
