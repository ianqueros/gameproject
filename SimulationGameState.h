#pragma once

#include "Global.h"
#include "Camera.h"
#include "MenuGameState.h"


class App;

class SimulationGameState : public GameState
{
private:
	TimerHandler * _timerPath;
	ALLEGRO_TIMER* simulationTimer;
	Camera* cam;
	EntityHandler* _entPath;
	int counter;
	int countTo;
	std::string userInput;
	char charInput[256];
	bool run;
	int x, y;

public:
	SimulationGameState(App* app, ControlHandler* control);
	~SimulationGameState();

	void update();
	void draw();
	void enter();
	void leave();
};

